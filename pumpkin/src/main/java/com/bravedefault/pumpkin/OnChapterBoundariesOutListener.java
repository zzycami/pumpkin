package com.bravedefault.pumpkin;

public interface OnChapterBoundariesOutListener {
    void onFirstPageOutEvent();
    void onLastPageOutEvent();
}
