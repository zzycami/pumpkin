package com.bravedefault.pumpkin;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import io.reactivex.functions.Action;

public class HorizontalPager extends ViewPager implements Pager {
    public HorizontalPager(@NonNull Context context) {
        super(context);
    }

    public HorizontalPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    private float startDragX = 0;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            if (ev.getAction() != 0 && MotionEvent.ACTION_MASK == MotionEvent.ACTION_DOWN) {

                if (getCurrentItem() == 0 || getCurrentItem() == getAdapter().getCount() - 1) {
                    startDragX = ev.getX();
                }
            }
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(ev);
    }

    @Override
    public void setOnChapterBoundariesOutListener(OnChapterBoundariesOutListener listener) {

    }

    @Override
    public void setOnPageChangeListener(Action onPageChanged) {

    }
}
