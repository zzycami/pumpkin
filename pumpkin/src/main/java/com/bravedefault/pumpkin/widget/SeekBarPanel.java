package com.bravedefault.pumpkin;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Seek bar progress panel, can display and operate page things
 */
public class SeekBarPanel extends LinearLayoutCompat{
    //Default Value
    private int defaultPageTextColor = Color.BLACK;
    private int defaultPageTextSize = 13;
    private int defaultPageTextWidth = 20;
    private int defaultPageButtonPadding = 20;

    // Attributes
    private int pageTextColor;
    private int pageTextSize;
    private int pageTextWidth;
    private int pageButtonPadding;
    private String leftText;
    private String rightText;

    // Views
    private ImageButton leftImageButton;
    private ImageButton rightImageButton;
    private TextView leftTextView;
    private TextView rightTextView;
    private SeekBar seekBar;

    public SeekBarPanel(Context context) {
        super(context);
        initSeekBarPanel(context, null, 0);
    }

    public SeekBarPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initSeekBarPanel(context, attrs, 0);
    }

    public SeekBarPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initSeekBarPanel(context, attrs, defStyleAttr);
    }

    private void initSeekBarPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.view_seekbar_panel, this);
        leftImageButton = findViewById(R.id.left_chapter);
        rightImageButton = findViewById(R.id.right_chapter);
        leftTextView = findViewById(R.id.left_page_text);
        rightTextView = findViewById(R.id.right_page_text);
        seekBar = findViewById(R.id.seekbar);

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SeekBarPanel);

            pageTextColor = typedArray.getColor(R.styleable.SeekBarPanel_pageTextColor, defaultPageTextColor);
            pageTextSize = typedArray.getInt(R.styleable.SeekBarPanel_pageTextSize, defaultPageTextSize);
            pageButtonPadding = typedArray.getInt(R.styleable.SeekBarPanel_pageButtonPadding, defaultPageButtonPadding);
            pageTextWidth = typedArray.getInt(R.styleable.SeekBarPanel_pageTextWidth, defaultPageTextWidth);
            leftText = typedArray.getString(R.styleable.SeekBarPanel_leftText);
            rightText = typedArray.getString(R.styleable.SeekBarPanel_rightText);

            leftTextView.setText(leftText);
            rightTextView.setText(rightText);

            leftTextView.setTextColor(pageTextColor);
            rightTextView.setTextColor(pageTextColor);
            leftTextView.setTextSize(pageTextSize);
            rightTextView.setTextSize(pageTextSize);

            leftImageButton.setPadding(pageButtonPadding, pageButtonPadding, pageButtonPadding, pageButtonPadding);
            rightImageButton.setPadding(pageButtonPadding, pageButtonPadding, pageButtonPadding, pageButtonPadding);
            leftTextView.setWidth(pageTextWidth);
            rightTextView.setWidth(pageTextWidth);
        }
    }
}
